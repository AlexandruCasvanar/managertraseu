-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Gazdă: 127.0.0.1
-- Timp de generare: ian. 19, 2020 la 10:13 PM
-- Versiune server: 10.1.37-MariaDB
-- Versiune PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Bază de date: `manager_traseu`
--

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `coordonates`
--

CREATE TABLE `coordonates` (
  `id` int(11) NOT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `locatieId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Eliminarea datelor din tabel `coordonates`
--

INSERT INTO `coordonates` (`id`, `lat`, `lon`, `locatieId`) VALUES
(1, 44.42774, 26.087277, NULL),
(2, 44.454377, 26.084153, NULL),
(3, 44.453144, 26.084617, NULL),
(4, 44.435952, 26.079412, NULL),
(5, 44.490795, 26.030823, NULL),
(6, 44.441381, 26.097236, NULL);

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `locaties`
--

CREATE TABLE `locaties` (
  `id` int(11) NOT NULL,
  `nume` varchar(255) DEFAULT NULL,
  `adresa` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Eliminarea datelor din tabel `locaties`
--

INSERT INTO `locaties` (`id`, `nume`, `adresa`) VALUES
(1, 'Palatul Parlamentului', 'Strada Izvor 2-4'),
(2, 'Muzeul National al Tarnului Roman', 'Soseaua Pavel D. Kiseleff 3'),
(3, 'Muzeul Antipa', 'Soseaua Pavel D. Kiseleff 1'),
(4, 'Opera Nationala Bucuresti', 'Bulevardul Mihail Kogalniceanu 70-72'),
(5, 'Palatul Victoria', 'Calea Victoriei'),
(6, 'Ateneul Roman', 'Strada Benjamin Franklin 1-3');

--
-- Indexuri pentru tabele eliminate
--

--
-- Indexuri pentru tabele `coordonates`
--
ALTER TABLE `coordonates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `locatieId` (`locatieId`);

--
-- Indexuri pentru tabele `locaties`
--
ALTER TABLE `locaties`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pentru tabele eliminate
--

--
-- AUTO_INCREMENT pentru tabele `coordonates`
--
ALTER TABLE `coordonates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pentru tabele `locaties`
--
ALTER TABLE `locaties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constrângeri pentru tabele eliminate
--

--
-- Constrângeri pentru tabele `coordonates`
--
ALTER TABLE `coordonates`
  ADD CONSTRAINT `coordonates_ibfk_1` FOREIGN KEY (`locatieId`) REFERENCES `locaties` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
