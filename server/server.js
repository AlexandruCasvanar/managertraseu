const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')

const sequelize = new Sequelize('manager_traseu', 'root', '', {
    dialect: 'mysql',
    define: {
        timestamps: false
    }
})

const Locatie = sequelize.define('locatie', {
    nume: Sequelize.STRING,
    adresa: Sequelize.STRING
}, {
    underscored: true
})

const Coordonata = sequelize.define('coordonate', {
    lat: Sequelize.DOUBLE,
    lon: Sequelize.DOUBLE
})

Locatie.hasMany(Coordonata)

const app = express()
app.use(bodyParser.json())

app.post('/create', async (req, res) => {
    try {
        await sequelize.sync({ force: true })
        res.status(201).json({ message: 'tabela creata cu succes' })
    } catch (err) {
        console.warn(err)
        res.status(500).json({ message: 'eroare server' })
    }
})

app.get('/locatii', async (req, res) => {
    try {
        let locatii = await Locatie.findAll()
        res.status(200).json(locatii)
    }
    catch (e) {
        console.warn(e)
        res.status(500).json({ message: 'serverul are o eroare' })
    }
})
app.post('/locatii', async (req, res) => {
    try {
        await Locatie.create(req.body)
        res.status(200).json({ message: 'locatie creata' })
    }
    catch (e) {
        console.warn(e)
        res.status(500).json({ message: 'serverul are o eroare' })
    }
})
app.get('/locatii/:id', async (req, res) => {
    try {
        let locatie = await Locatie.findByPk(req.params.id)
        if (locatie) {
            res.status(200).json(locatie)
        }
        else {
            res.status(404).json({ message: 'locatia cu id-ul precizat nu a fost gasita' })
        }
    }
    catch (e) {
        console.warn(e)
        res.status(500).json({ message: 'serverul are o eroare' })
    }
})
app.put('/locatii/:id', async (req, res) => {
    try {
        let locatie = await Locatie.findByPk(req.params.id)
        if (locatie) {
            await locatie.update(req.body)
            res.status(202).json({ message: "modificare facuta" })
        }
        else {
            res.status(404).json({ message: 'locatia cu id-ul precizat nu a fost gasita' })
        }
    }
    catch (e) {
        console.warn(e)
        res.status(500).json({ message: 'serverul are o eroare' })
    }
})
app.delete('/locatii/:id', async (req, res) => {
    try {
        let locatie = await Locatie.findByPk(req.params.id)
        if (locatie) {
            await locatie.destroy()
            res.status(202).json({ message: "locatie stearsa" })
        }
        else {
            res.status(404).json({ message: 'locatia cu id-ul precizat nu a fost gasita' })
        }
    }
    catch (e) {
        console.warn(e)
        res.status(500).json({ message: 'serverul are o eroare' })
    }
})

app.get('/locatii/:lid/coordonate', async (req, res) => {
    try {
        let locatie = await Locatie.findByPk(req.params.lid)
        if (locatie) {
            let coordonata = await locatie.getCoordonate({ where: { id: req.params.cid } })
            await locatie.update(req.body)
            res.status(200).json(coordonata)
        }
        else {
            res.status(404).json({ message: 'locatia cu id-ul precizat nu a fost gasita' })
        }
    }
    catch (e) {
        console.warn(e)
        res.status(500).json({ message: 'serverul are o eroare' })
    }
})

app.post('/locatii/:lid/coordonate', async (req, res) => {
    try {
        let locatie = await Locatie.findByPk(req.params.lid)
        if (locatie) {
            let coordonata = req.body
            coordonata.locatie_id = locatie.id
            await Coordonata.create(coordonata)
            res.status(201).json({ message: 'coordonata creata' })
        }
        else {
            res.status(404).json({ message: 'locatia cu id-ul precizat nu a fost gasita' })
        }
    }
    catch (e) {
        console.warn(e)
        res.status(500).json({ message: 'serverul are o eroare' })
    }
})

app.put('/locatii/:lid/coordonate', async (req, res) => {
    try {
        let locatie = await Locatie.findByPk(req.params.lid)
        if (locatie) {
            let coordonte = await locatie.getCoordonate({ where: { id: req.params.cid } })
            let coordonata = coordonate.shift()
            if (coordonata) {
                await coordonata.update(req.body)
                res.status(202).json({ message: 'coordonata modificata' })
            }
        }
        else {
            res.status(404).json({ message: 'locatia cu id-ul precizat nu a fost gasita' })
        }
    }
    catch (e) {
        console.warn(e)
        res.status(500).json({ message: 'serverul are o eroare' })
    }
})

app.delete('/locatii/:lid/coordonate', async (req, res) => {
    try {
        let locatie = await Locatie.findByPk(req.params.lid)
        if (locatie) {
            let coordonate = await locatie.getCoordonate({ where: { id: req.params.cid } })
            let coordonata = coordonate.shift()
            if (coordonata) {
                await coordonata.destroy(req.body)
                res.status(202).json({ message: 'coordonate sterse cu succes' })
            }
        }
        else {
            res.status(404).json({ message: 'locatia cu id-ul precizat nu a fost gasita' })
        }
    }
    catch (e) {
        console.warn(e)
        res.status(500).json({ message: 'serverul are o eroare' })
    }
})



app.listen(8080)