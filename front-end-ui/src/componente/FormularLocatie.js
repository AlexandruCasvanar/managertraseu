import React, {Component} from 'react'

class FormularLocatie extends Component{
    constructor(props){
        super(props)
        this.state = {
            nume : '',
            adresa : ''
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }

    }
    render(){
        return <div>
            <input type="text" placeholder="nume" onChange={this.handleChange} name="nume" />
            <input type="text" placeholder="adresa" onChange={this.handleChange} name="adresa" />
            <input type="button" value="adauga" onClick={this.add} />
        </div>
    }
}

export default FormularLocatie