import React,{Component} from 'react'
import MagazieLocatie from '../store/MagazieLocatie'
import FormularLocatie from './FormularLocatie'
import Locatie from './Locatie'

class ListaLocatie extends Component{
    constructor(){
        super()
        this.state = {
            locatii : []
        }
        this.store = new MagazieLocatie()
        this.add = (locatie) => {
            this.store.addLocatie(locatie)
        }
        this.sterge = (id) =>{
            this.store.stergeLocatie(id)
        }
        this.salveaza = (id, locatie) =>{
            this.store.salveazaLocatie(id, locatie)
        }
    }
    componentDidMount(){
        this.store.getLocatii()
        this.store.emitter.addListener('LOCATII_ADAUGATE_CU_SUCCES', () => {
            this.setState({
                locatii : this.store.locatii
            })
        })
    }
    render(){
        return <div>
        {
            this.state.locatii.map((e, i) => <Locatie key={i} item={e} onSave={this.salveaza} onDelete={this.sterge} />)
        }
        <FormularLocatie onAdd={this.adauga} />
        </div>
    }
}

export default ListaLocatie