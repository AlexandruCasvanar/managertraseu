import React, { Component } from 'react';
import MagazieCoordonate from '../store/MagazieCoordonate'
import Coordonata from './Coordonata'
import FormularCoordonata from './FormularCoordonata.js'

class LocatieChild extends Component {
	constructor(props){
		super(props)
		this.state = {
			coordonate : []
		}
		this.store = new MagazieCoordonate()
		this.emitter = this.store.emitter
		this.add = (coordonata) => {
			this.store.addCoordonata(this.props.item.id, coordonata)
        }
        this.save = (coordonataId, coordonata) => {
			this.store.salveazaCoordonata(this.props.item.id, coordonataId, coordonata)
		}
		this.delete = (coordonataId) => {
			this.store.stergeCoordonata(this.props.item.id, coordonataId)
		}

	}
	componentDidMount(){
		this.store.getCoordonate(this.props.item.id)
		this.store.emitter.addListener('COORDONATE_INCARCATE_CU_SUCCES', () => {
			this.setState({
				coordonate : this.store.content
			})
		})
	}
  render() {
    return (
      <div>
        <h3>Coordonte {this.props.item.title}</h3>
        <div>
        	{
        		this.state.coordonate.map((e, i) => <Coordonata item={e} key={i} onDelete={this.delete} onSave={this.save} />)
        	}
        </div>
        <div>
        	<FormularCoordonata onAdd={this.add} />
        </div>
        <form>
        	<input type="button" value="back" onClick={() => this.props.onCancel()} />
        </form>
      </div>
    )
  }
}

export default LocatieChild