import React, { Component } from 'react';

class Coordonata extends Component {
	constructor(props){
		super(props)
		this.state = {
			isEditing : false,
			lat : this.props.item.lat,
			lon : this.props.item.lon
		}
		this.handleChange = (evt) => {
			this.setState({
				[evt.target.name] : evt.target.value
			})
		}
	}
  render() {
  	if (!this.state.isEditing){
	    return (
	      <div>
	             Latitudine {this.props.item.lat} Longitudine {this.props.item.lon}
	          <input type="button" value="delete" onClick={() => this.props.onDelete(this.props.item.id)} />
          	<input type="button" value="edit" onClick={() => this.setState({isEditing : true})} />
	      </div>
	    )  		
  	}
  	else{
  		return (
  			<div>
		 			Latitudine
		 			<input type="text" name="lat" id="lat" onChange={this.handleChange} value={this.state.lat}/>
		 			Longitudine 
		 			<input type="text" name="lon" id="lon" onChange={this.handleChange} value={this.state.lon}/>
		      <input type="button" value="save" onClick={() => {
		      		this.props.onSave(this.props.item.id, {
		      	lat : this.state.lat,
		      	lon : this.state.lon
		      })
		      		this.setState({isEditing : false})
		      	}
		      } />
		    	<input type="button" value="cancel" onClick={() => this.setState({isEditing : false})} />
	    	</div>
  		)
  	}
  }
}

export default Coordonata