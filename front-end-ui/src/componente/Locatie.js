import React,{Component} from 'react'

class Locatie extends Component{
    
    constructor(props){
        super(props)
        this.state={
            isEditing : false,
            nume: this.props.item.nume,
            adresa : this.props.item.adresa
        }
        this.sterge = () =>{
            this.props.onDelete(this.props.item.id)
        }
        this.editeaza = () =>{
            this.setState({isEditing: true})
        }
        this.anuleaza = () =>{
            this.setState({isEditing: false})
        }
        this.handleChange=(evt) =>{
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        this.salveaza = () =>{
            this.props.onSave(this.props.item.id,{
                nume: this.state.nume,
                adresa: this.state.adresa
            })
            this.setState({
                isEditing: false
            })
        }
    }
    
    render(){
        if(this.state.isEditing){
            return <div>
            <div>
                <h3><input type="text" value={this.state.nume} name="nume" onChange={this.handleChange} /></h3>
                <h5><input type="text" value={this.state.adresa} name="adresa" onChange={this.handleChange} /></h5>
            </div>    
            <div>
                <input type="button" value="anuleaza" onClick={this.anuleaza} />
                <input type="button" value="salveaza" onClick={this.salveaza} />
            </div>
            </div>
        }
        else{
            return <div>
                <h3>{this.props.item.nume}</h3>
                <h5>{this.props.item.adresa}</h5>
                <div>
                    <input type="button" value="sterge" onClick={this.sterge} />
                    <input type="button" value="editeaza" onClick={this.editeaza} />
                </div>
            </div>
        }
    }
}
export default Locatie