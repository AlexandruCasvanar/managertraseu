import React, { Component } from 'react';

class FormularCoordonata extends Component {
	constructor(props){
		super(props)
		this.state = {
			lat : '',
			lon : ''
		}
		this.handleChange = (evt) => {
			this.setState({
				[evt.target.name] : evt.target.value
			})
		}
	}
  render() {
    return (
      <div>
        <form>
        	<label htmlFor="lat">Latitudine</label>
        	<input type="text" name="lat" id="lat" onChange={this.handleChange}/>
        	<label htmlFor="lon">Longitudine</label>

        	<input type="text" name="lon" id="lon" onChange={this.handleChange}/>
        	<input type="button" value="+" onClick={() => this.props.onAdd({
        		lat : this.state.lat,
        		lon : this.state.lon
        	})}/>
        </form>
      </div>
    )
  }
}

export default FormularCoordonata
