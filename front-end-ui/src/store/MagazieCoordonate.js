import axios from 'axios'
import {EventEmitter} from 'fbemitter'

const SERVER = 'http://localhost:8080'

class MagazieCoordonate{
	constructor(){
		this.emitter = new EventEmitter()
		this.content = []
		this.selected = null
	}
	async addCoordonata(locatieId, coordonata){
		try{
			let response = await axios.post(`${SERVER}/locatii/${locatieId}/coordonate`, coordonata)
			await this.getCoordonate(locatieId)
			this.emitter.emit('COORDONATA_ADD_CU_SUCCES')
		}
		catch(ex){
			console.warn(ex)
			this.emitter.emit('EROARE_ADAUGARE_COORDONATA')
		}
	}
	async getCoordonate(locatieId){
		try{
			let response = await axios(`${SERVER}/locatii/${locatieId}/coordonate`)
			this.content = response.data
			this.emitter.emit('COORDONATE_INCARCATE_CU_SUCCES')
		}
		catch(ex){
			console.warn(ex)
			this.emitter.emit('EROARE_LA_INCARCARE')
		}
    }
    async salveazaCoordonata(locatieId, coordonataId, coordonata){
        try {
            await axios.put(`${SERVER}/locatii/${locatieId}/coordonate/${coordonataId}`, coordonata)
            await this.getCoordonate(locatieId)
            this.emitter.emit('ALVATA_CU_SUCCES')
        } catch (e) {
            console.warn(e)
            this.emitter.emit('EROARE_LA_SALVARE')
        }
	}
	async stergeCoordonata(locatieId, coordonataId){
        try {
            await axios.delete(`${SERVER}/locatii/${locatieId}/coordonate/${coordonataId}`)
            await this.getCoordonate(locatieId)
            this.emitter.emit('COORDONATA_STEARSA_CU_SUCCESS')
        } catch (e) {
            console.warn(e)
            this.emitter.emit('EROARE_LA STERGERE')
        }
	}

	async getOne(locatieId, coordonataId){
        try {
            let response = await axios(`${SERVER}/locatii/${locatieId}/coordonate/${coordonataId}`)
            this.selected = response.data
            this.emitter.emit('COORDONATA_INCARCATA_CU_SUCCES')
        } catch (e) {
            console.warn(e)
            this.emitter.emit('ERORE_LA_INCARCARE_COORDONATA')
        }
	}

}

export default MagazieCoordonate