import axios from 'axios'
import {EventEmitter} from 'fbemitter'
const SERVER = 'http://localhost:8080'

class MagazieLocatie{
	constructor(){
		this.emitter = new EventEmitter()
		this.content = []
		this.selected = null
	}
	async addLocatie(locatie){
		try{
			let raspuns = await axios.post(`${SERVER}/locatii`, locatie)
			await this.getLocatii()
			this.emitter.emit('LOCATIE_ADAUGATA_CU_SUCCES')
		}
		catch(ex){
			console.warn(ex)
			this.emitter.emit('EROARE_ADAUGARE')
		}
    }
    
    async getLocatie(id){
        try {
            let response = await axios(`${SERVER}/locatii/${id}`)
            this.selected = response.data
            this.emitter.emit('LOCATIE_INCARCATA_CU_SUCCES')
        } catch (e) {
            console.warn(e)
            this.emitter.emit('EROARE_LA_INCARCARE')
        }
	}
	async getLocatii(){
		try{
			let response = await axios(`${SERVER}/locatii`)
			this.content = response.data
			this.emitter.emit('LOCATII_INCARCATE_CU_SUCCES')
		}
		catch(ex){
			console.warn(ex)
			this.emitter.emit('EROARE_INCARCARE_LOCATII')
		}
    }
    
    async salveazaLocatie(id, locatie){
        try {
            await axios.put(`${SERVER}/locatii/${id}`, locatie)
            await this.getLocatii()
            this.emitter.emit('LOCATIE_SALVATA_CU_SUCCES')
        } catch (e) {
            console.warn(e)
            this.emitter.emit('SAVE_ERROR')
        }
    }
    
	async stergeLocatie(id){
        try {
            await axios.delete(`${SERVER}/locatii/${id}`)
            await this.getLocatii()
            this.emitter.emit('LOCATIE_STEARSA')
        } catch (e) {
            console.warn(e)
            this.emitter.emit('EROARE_LA_STERGERE')
        }
	}

}

export default MagazieLocatie;